<?php
/* Template Name: Página de Sucursales */
?>
<?php get_header(); ?>
<?php if ( have_posts() ): ?>
<div id="post-list">

	<?php echo do_shortcode('[block id="header-sucursales"]'); ?>

<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div class="section-content relative" style="margin-bottom: 150px;">
		<div class="row data-container">
			
		</div>
	</div>
<?php endwhile; ?>

</div>

<?php endif; ?>

<script type="text/javascript">
	
	jQuery(document).ready(function($) {
		
		var slug				=	'region_comuna';
		var comunasPorRegion	=	new Array();

		var flagTest = false;
		$.ajax({
			type: 'GET',
			cache: true,
			url: '/wp-json/wp/v2/region_comuna?per_page=100',
			dataType: 'json',
			success: function(posts) {

				comunasPorRegion = posts;

                $.each(posts, function(index, post) {

					if( post.parent == 0){
						$('#region').append('<option value="' + post.id + '">' + post.name + '</option>');
					}else{
						$('#comunas').append('<option value="' + post.id + '">' + post.name + '</option>');
					}
                    

                    var str2table = '<div class="comunas hidden" id="comuna-'+post.id+'" data-slug="' + post.slug + '" data-name="' + post.name + '" >';
                    str2table += '<h2>'+post.name+'</h2>';

                    str2table += '<table id="table-comuna-'+post.id+'"><thead>';
                    str2table += '	<tr><td>Nombre</td><td>Direccion</td><td>Estado</td><td>Horario</td></tr>';
                    str2table += '</thead><tbody></tbody></table>';
                    
                    str2table += '</div>';

					$(".data-container").append( str2table );

                });
                flagTest = true;
                console.log(flagTest);

                for (var i = 1; i <= 2; i++) {
					console.log(" Se terminó de procesar las comunas y regiones? -> " + flagTest + ", ciclo " + i);
					$.ajax({
						type: 'GET',
						cache: true,
						url: '/wp-json/wp/v2/sucursales?order=asc&orderby=id&page='+i+'&per_page=100', //url: '/wp-json/wp/v2/sucursales?filter[region_comuna]='+post.slug,
						dataType: 'json',
						success: function(sucursales) {


							$.each(sucursales, function(index, sucursal) {

								// Nombre, Direccion, Estado, Horario
								var elTr = '<tr>';
										elTr += '<td>'+sucursal.title.rendered+'</td>';
										elTr += '<td>'+sucursal.acf.direccion+'</td>';
										elTr += '<td>'+sucursal.acf.estado+'</td>';
										elTr += '<td>'+sucursal.acf.horario+'</td>';
									elTr += '</tr>';
								
								$("#table-comuna-"+sucursal.region_comuna[0]+" tbody").append(elTr);
							});

						},
						error: function (request, status, error) {
							console.log(request.responseText);
						}
					});
				}

			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});


		$("#comunas").on('change', function(event) {
			event.preventDefault();

			var $idRegion = this.value;

			$(".comunas").addClass('hidden')
			//$(".comunas").find("[data-slug='" + $idRegion + "']").removeClass('hidden');
			$("#comuna-" + $idRegion).removeClass('hidden');


		});

		$("#region").on('change', function(event) {
			event.preventDefault();
			var $idRegion = this.value;
			$("#comunas option").remove();
			$('#comunas').append('<option value="">-- Selecciona tu comuna --</option>');

			$.each(comunasPorRegion, function(index, comuna) {
				 
				 if( $idRegion == comuna.parent ){
				 	$('#comunas').append('<option value="' + comuna.id + '">' + comuna.name + '</option>');

				 	console.log( $idRegion + " == " + comuna.parent );
				 } else {
				 	console.log( $idRegion + " != " + comuna.parent );
				 }

				 console.log(comuna);
				 console.log("| ================== |");
			});

		});

		console.log("comunasPorRegion");
		console.log(comunasPorRegion);
		
	});
</script>
<?php get_footer(); ?>
<?php
// Add custom Theme Functions here

/**
 * Code goes in theme functions.php
 */
add_action( 'init', function () {
	if ( function_exists( 'add_ux_builder_post_type' ) ) {
		add_ux_builder_post_type( 'sucursales' );
	}
} );
<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>

<div id="content" class="blog-wrapper blog-single page-wrapper">
	
	<?php echo do_shortcode('[block id="header-single-beneficios"]'); ?>
	<?php
	do_action('flatsome_before_blog');
?>
<div class="row align-center">
	<div class="large-10 col">
		<?php if ( have_posts() ) : ?>

		<?php /* Start the Loop */ ?>

		<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="article-inner <?php flatsome_blog_article_classes(); ?>">
				<?php
					if(flatsome_option('blog_post_style') == 'default' || flatsome_option('blog_post_style') == 'inline'){
						get_template_part('template-parts/beneficios/partials/entry-image', flatsome_option('blog_posts_header_style') );
					}
				?>
				<?php get_template_part( 'template-parts/beneficios/content', 'single' ); ?>
			</div>
		</article>

		<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif; ?>
	</div>
</div>

<?php do_action('flatsome_after_blog'); ?>

</div>

<?php get_footer();

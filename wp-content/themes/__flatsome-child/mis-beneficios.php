<?php
/* Template Name: Página de Mis beneficios */
?>
<?php get_header(); ?>
<?php if ( have_posts() ): ?>
<div id="post-list">

<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

	<div class="section-content relative" style="margin-bottom: 150px;">		
		<div class="row data-container loading row-full-width align-middle align-center" id="beneficios">
			
			<div class="row" >
				<div class="col small-12 large-6">
					
					<div class="">
						<!--<ul>
							<li class="active">
								<a class="filterCats" href="#" rel="">Todas</a>
							</li>
						</ul>-->
						<select name="filterCats" class="filterCats" id="filterCats">
							<option value="">Todos</option>
							
							<?php
							$taxonomy = 'categoria_beneficios';
							$taxonomy_terms = get_terms( $taxonomy, array(
								'hide_empty' => 0,
							) );
							foreach($taxonomy_terms as $term){
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>';

							}
							
							?>
						</select>

					</div>				
				</div>

				<div class="col small-12 large-6">
					<div class="buscador">

						<form action="">
							<input type="text" name="buscarbeneficio" id="buscarbeneficio" />
						</form>
						
					</div>
				</div>
			</div>

			<?php get_template_part( 'template-parts/beneficios/loop/beneficios' ); ?>
		</div>
	</div>
<?php endwhile; ?>

</div>

<?php endif; ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {		

		$("#filterCats").on('change', function(event) {
			
			event.preventDefault();
			//$("a.filterCats").removeClass('active');
			//$(this).addClass('active');

			var $currentCat = $(this).children("option:selected").val(); //$(this).attr('rel');

			if( $currentCat.length > 0 ){
				$(".caja-beneficio").addClass('hidden');
				$(".caja-beneficio.categoria-" + $currentCat).removeClass('hidden');					
			}else{
				$(".caja-beneficio").removeClass('hidden');
			}
			
		});


		$("#buscarbeneficio").on('keyup', function(event) {
			event.preventDefault();
			var searchValue = $(this).val();

			if( searchValue.length > 3 ){
console.log(searchValue);

				$(".caja-beneficio").addClass('hidden');

				$(".caja-beneficio").each(function(){

console.log( "===========" );
console.log( $(this).text() );
console.log( $(this).text().search(searchValue) );
console.log( "===========" );
					//if( $(this).text().indexOf(searchValue) > -1){
					if( $(this).text().search(searchValue) > -1){
						$(this).removeClass('hidden');
					}

				});				

			}else{

				$(".caja-beneficio").removeClass('hidden');

			}


		});

		/*$.ajax({
			type: 'GET',
			cache: true,
			url: '/wp-json/wp/v2/categoria_beneficios?per_page=100',
			dataType: 'json',
			success: function(cat_beneficios) {

				console.log(cat_beneficios);

                $.each(cat_beneficios, function(index, catBen) {

                    var str2div = '<li id="beneficio-'+catBen.id+'" >';
                    str2div += '<a class="filterCats" href="#'+catBen.slug+'">'+catBen.name+'</a>';
                    str2div += '</li>';

                    $(".categorias ul").append(str2div);

					//$(".categorias ul").append( $('li').addClass( 'beneficio-'+catBen.id ).add('a').addClass('filterCats').html(catBen.name).attr('href', catBen.slug) );

                });

			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});*/




	});
	
</script>


<?php get_footer(); ?>
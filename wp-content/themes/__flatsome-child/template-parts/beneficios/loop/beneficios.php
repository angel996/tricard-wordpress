<?php
			$args = array(  
		        'post_type'			=> 'beneficios',
		        'post_status'		=> 'publish',
		        'posts_per_page'	=> -1, 
		        'orderby'			=> 'title', 
		        'order'				=> 'ASC', 
		    );
		    $loop = new WP_Query( $args ); 
			?>
			<div class="row cajas-beneficios">
				<?php
					while ( $loop->have_posts() ) : $loop->the_post();

					$categoria_beneficios = get_the_terms( get_the_ID(), 'categoria_beneficios' ); 
					$strCatBen = '';
					foreach ($categoria_beneficios as $cat_ben) {
						$strCatBen .= " categoria-" . $cat_ben->slug;
					}
						
				?>
				<div class="col col-cat medium-3 small-12 large-3 caja-beneficio categoria-<?php echo $strCatBen; ?>" data-animate="fadeInDown" data-animated="true">
					<div class="col-inner box-shadow-2 box-shadow-1-hover parallax-active" data-parallax-fade="true" data-parallax="1" style="transform: translate3d(0px, -6.57px, 0px); backface-visibility: hidden; opacity: 0.94;">
						<div class="img has-hover x md-x lg-x y md-y lg-y img-cat" >
							<div data-animate="fadeInDown" data-animated="true">
								<div class="img-inner dark">
									<img width="150" height="150" src="<?php the_post_thumbnail_url('medium'); ?>" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
								</div>
							</div>
							
						</div>
						<div class="text text-cat">
							<h3 class="p1"><?php print the_title(); ?></h3>
							<p><?php print the_excerpt(); ?></p>
						</div>
						<a href="<?php the_permalink(); ?>" data-animate="fadeInDown" class="button alert is-large box-shadow-2 box-shadow-1-hover expand max-90" style="border-radius:5px;" data-animated="true">
							<span>Saber más</span>
						</a>
					</div>
				</div>
				<?php
					endwhile;
					wp_reset_postdata(); 
				?>
			</div><!-- .row.cajas-beneficios -->

<style>
	.row.cajas-beneficios .col-cat>.col-inner {
		padding: 0px 0px 10px 0px;
		margin: 5px 10px 5px 10px;
		border-radius: 10px;
	}
	
	.row.cajas-beneficios .text-cat {
		text-align: center;
		padding: 5px 10px;
	}
	.row.cajas-beneficios .img-cat {
		width: 100%;
	}

	.row.cajas-beneficios .img-cat img{
		margin-top: 0;
		z-index: -1;
		border-radius: 10px 10px 0 0;
		border-bottom: 1px solid #A4A4A4;
		min-height: 90px;
	}

	#beneficios{
		padding-top: 30px;
		padding-bottom: 40px;
	}

	a.max-90{
		background-color: #D41517;
		max-width: 90% !important;
		margin: 0 auto 10px;
	}

</style>



<?php

	$meta_filter = FALSE;
	$args = array(
		'post_type'			=> 'beneficios',
		'post_status'		=> 'publish',
		'posts_per_page'	=> 8,
		'paged'			=> get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'orderby'		=> 'title',
		'order'			=> 'ASC',
	);

	if(isset($_GET["buscarbeneficio"]) && $_GET["buscarbeneficio"])
	{
		$meta_filter = TRUE;
		$args['meta_compare'] = "LIKE";
		$args['post_title'] = $_GET["buscarbeneficio"];
		
		add_filter( 'posts_where', 'title_filter', 10, 2 );
	}
	
	if(isset($_GET["filterCats"]) && $_GET["filterCats"])
	{	  
		$args['tax_query'][] = array(
            'taxonomy' => 'categoria_beneficios',
            'field'    => 'term_id',
            'terms'    => $_GET["filterCats"],
		);
	}

	$loop = new WP_Query( $args );

	if($meta_filter)
	{
		remove_filter( 'posts_where', 'title_filter', 10, 2 );
	}

?>

<div class="hide-for-medium">
	<div class="row cajas-beneficios">
		<?php
			while ( $loop->have_posts() ) : $loop->the_post();

			$categoria_beneficios = get_the_terms( get_the_ID(), 'categoria_beneficios' );
			$strCatBen = '';

			if(isset($categoria_beneficios) && is_array($categoria_beneficios))
			{
				foreach ($categoria_beneficios as $cat_ben) {
					$strCatBen .= " categoria-" . $cat_ben->slug;
				}
			}
		?>

		<div class="col col-cat medium-3 small-11 large-3 caja-beneficio categoria-<?php echo $strCatBen; ?>" data-animate="fadeInDown" data-animated="true">
			<div class="col-inner box-shadow-2 box-shadow-1-hover">
				<div class="img has-hover x md-x lg-x y md-y lg-y img-cat" >
					<div data-animate="fadeInDown" data-animated="true">
						<div class="img-inner dark">
							<img width="150" height="150" src="<?php the_post_thumbnail_url('square'); ?>" class="attachment-thumbnail size-thumbnail" alt="" loading="lazy">
						</div>
					</div>
				</div>
				<div class="text text-cat">
					<h3 class="p1"><?php print the_title(); ?></h3>
					<p><?php print get_field( "bajada" ); ?></p>
				</div>
				<a href="<?php the_permalink(); ?>" data-animate="fadeInDown" class="max-90" style="border-radius:5px;" data-animated="true">
					<span>Conocer beneficio</span>
				</a>
			</div>
		</div>
						
		<?php endwhile; ?>
	</div><!-- .row.cajas-beneficios -->

	<div class="paginator-beneficios col col-cat medium-12 small-12 large-12">
		<div style="text-align:center">
			<?php
				// get the current page
				if ( !$current_page = get_query_var('paged') )
					$current_page = 1;

				if( get_option('permalink_structure') ) {
					$format = '?paged%#%';
				}
				echo str_replace("<li><span", "<li class='current-page'><span", paginate_links(array(
					'base'	   => @add_query_arg('paged','%#%'),
					'format'   => $format,
					'current'  => $current_page,
					'total'    => $loop->max_num_pages,
					'mid_size' => 4,
					'prev_text' => '<img src="'.get_home_url().'/wp-content/uploads/2021/04/chevron-double-left-light.svg">',
					'next_text' => '<img src="'.get_home_url().'/wp-content/uploads/2021/04/chevron-double-right-light.svg">',
					'type'=>'list'
				)));
			?>
		</div>
	</div>
		
	<?php wp_reset_postdata(); ?>
</div>

<div class="show-for-medium">
	<div class="row cajas-beneficios">
		<?php
			while ( $loop->have_posts() ) : $loop->the_post();

			$categoria_beneficios = get_the_terms( get_the_ID(), 'categoria_beneficios' );
			$strCatBen = '';
			foreach ($categoria_beneficios as $cat_ben) {
				$strCatBen .= " categoria-" . $cat_ben->slug;
			}
		?>

		<div class="col medium-12 small-12 large-12 caja-beneficio categoria-<?php echo $strCatBen; ?>">
			<div class="col-inner benefits-mobile-box-shadow" style="padding-bottom:10px;">
				<div class="img has-hover x md-x lg-x y md-y lg-y img-cat" >
					<div class="img-inner dark">
						<img width="150" height="150" src="<?php the_post_thumbnail_url('square'); ?>" class="attachment-thumbnail size-thumbnail">
					</div>
				</div>
				<div class="text text-cat">
					<h3 class="p1"><?php print the_title(); ?></h3>
					<p><?php print get_field( "bajada" ); ?></p>
				</div>
				<a href="<?php the_permalink(); ?>" data-animate="fadeInDown" class="max-90" style="font-size:18px; border-radius:5px; color:#f30000" data-animated="true">
					<span>Conocer beneficio</span>
				</a>
			</div>
		</div>
						
		<?php endwhile; ?>
	</div><!-- .row.cajas-beneficios -->

	<div class="paginator-beneficios col col-cat medium-12 small-12 large-12">
		<div style="text-align:center">
			<?php
				// get the current page
				if ( !$current_page = get_query_var('paged') )
					$current_page = 1;

				if( get_option('permalink_structure') ) {
					$format = '?paged=%#%';
				}
				echo str_replace("<li><span", "<li class='current-page'><span", paginate_links(array(
					'base'     => @add_query_arg('paged','%#%'),
					'format'   => $format,
					'current'  => $current_page,
					'total'    => $loop->max_num_pages,
					'mid_size' => 4,
					'prev_text' => '<img src="'.get_home_url().'/wp-content/uploads/2021/04/chevron-double-left-light.svg">',
					'next_text' => '<img src="'.get_home_url().'/wp-content/uploads/2021/04/chevron-double-right-light.svg">',
					'type'=>'list'
				)));
			?>
		</div>
	</div>
		
	<?php wp_reset_postdata(); ?>
</div>



<?php
/* Template Name: Página de Mis beneficios */
?>

<?php get_header(); ?>
<?php if ( have_posts() ): ?>
<div id="post-list">

<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

	<div class="section-content relative" style="margin-bottom: 150px;">		
		<div class="row data-container loading row-full-width align-middle align-center" id="beneficios">
			
			<div class="row" >
				<div class="col small-12 large-12">
					
					<div class="categorias">
						<ul>
							<li class="active">
								<a class="filterCats" href="#" rel="">Todas</a>
							</li>
							<?php
							$taxonomy = 'categoria_beneficios';
							$taxonomy_terms = get_terms( $taxonomy, array(
								'hide_empty' => 0,
							) );
							foreach($taxonomy_terms as $term){
								echo '<li><a class="filterCats" href="#'.$term->slug.'" rel="'.$term->slug.'">'.$term->name.'</a></li>';

							}
							
							?>
						</ul>
					</div>				
				</div>
			</div>

			<?php get_template_part( 'template-parts/beneficios/loop/beneficios' ); ?>
		</div>
	</div>
<?php endwhile; ?>

</div>

<?php endif; ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {		

		$("a.filterCats").on('click', function(event) {
			
			event.preventDefault();
			$("a.filterCats").removeClass('active');
			$(this).addClass('active');

			var $currentCat = $(this).attr('rel');

			if( $currentCat.length > 0 ){
				$(".caja-beneficio").addClass('hidden');
				$(".caja-beneficio.categoria-" + $currentCat).removeClass('hidden');					
			}else{
				$(".caja-beneficio").removeClass('hidden');
			}
			
		});

		/*$.ajax({
			type: 'GET',
			cache: true,
			url: '/wp-json/wp/v2/categoria_beneficios?per_page=100',
			dataType: 'json',
			success: function(cat_beneficios) {

				console.log(cat_beneficios);

                $.each(cat_beneficios, function(index, catBen) {

                    var str2div = '<li id="beneficio-'+catBen.id+'" >';
                    str2div += '<a class="filterCats" href="#'+catBen.slug+'">'+catBen.name+'</a>';
                    str2div += '</li>';

                    $(".categorias ul").append(str2div);

					//$(".categorias ul").append( $('li').addClass( 'beneficio-'+catBen.id ).add('a').addClass('filterCats').html(catBen.name).attr('href', catBen.slug) );

                });

			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});*/




	});
	
</script>


<?php get_footer(); ?>
<?php

// Add custom Theme Functions here		

	function title_filter( $where, &$wp_query ){
		global $wpdb;
		if ( $search_term = $wp_query->get( 'post_title' ) ) {
			$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
		}

		return $where;
	}

	function parent_filter( $where, &$wp_query ){
		global $wpdb;
		if ( $search_term = $wp_query->get( 'post_parent' ) ) {
			$where .= ' AND ' . $wpdb->posts . '.post_parent LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
		}

		return $where;
	}
	function wpbody_sin_mensajes() {
		return "Datos de acceso <b>incorrectos</b>";
	}
	add_filter('login_errors', 'wpbody_sin_mensajes');
	

/**
 * Code goes in theme functions.php
 */
add_action( 'init', function () {
	if ( function_exists( 'add_ux_builder_post_type' ) ) {
		add_ux_builder_post_type( 'sucursales' );
	}
} );
add_action( 'init', function () {
	if ( function_exists( 'add_ux_builder_post_type' ) ) {
		add_ux_builder_post_type( 'mis_seguros' );
	}
} );


add_image_size( 'square', 220, 220, array( 'center', 'center' ) );
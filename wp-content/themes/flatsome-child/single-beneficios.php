<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>
<?php echo do_shortcode('[block id="header-single-beneficios"]'); ?>
<div id="content" class="blog-wrapper blog-single page-wrapper">
	<?php
		do_action('flatsome_before_blog');
	?>
<section class="section-page-benefits-single">
	<div class="row align-center benefit">
		<div class="large-12">
			<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="article-inner <?php flatsome_blog_article_classes(); ?>">

					<div class="single-post-entry-img" 
						 style="background:url(<?php print_r(get_field( "portada" )["url"])?>) no-repeat; background-position: 0 4%; background-size:cover;">
					</div>

					<?php get_template_part( 'template-parts/beneficios/content', 'single' ); ?>
				</div>
			</article>

			<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>
		</div>
	</div>
</section>

<?php do_action('flatsome_after_blog'); ?>

</div>

<?php get_footer();

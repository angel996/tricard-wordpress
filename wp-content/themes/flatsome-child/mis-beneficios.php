<?php
/* Template Name: Página de Mis beneficios */
?>
<?php get_header(); ?>

<?php if ( have_posts() ): ?>

<div id="post-list">
	
<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

	<div class="section-content relative" style="margin-bottom: 150px;">
		<div class="row data-container loading align-middle align-center" id="beneficios">

			<div class="col small-12 large-12 medium-12 hide-for-medium">
				<form action="" method="GET">
					<div class="row">
						<div class="col small-12 medium-6 large-6">
							<div class="select-container" style="max-width: 350px;" >

								<select name="filterCats" class="filterCats" id="filterCats" style="float:left;">
									<option value="">Todos</option>
									<?php
									$taxonomy = 'categoria_beneficios';
									$taxonomy_terms = get_terms( $taxonomy, array(
										'hide_empty' => 0,
									) );

									foreach($taxonomy_terms as $term){
										if(isset($_GET["filterCats"]) && $_GET["filterCats"] == $term->term_id)
										{
											echo '<option value="'.$term->term_id.'" selected="selected">'.$term->name.'</option>';
										}
										else
										{
											echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';	
										}
									}
									?>
								</select>
							</div>
						</div>
						<?php
							//$placeholder = isset($_POST["buscarbeneficio"]) ? $_POST["buscarbeneficio"] : "Buscar beneficio";
							$placeholder = isset($_GET["buscarbeneficio"]) ? $_GET["buscarbeneficio"] : "Buscar beneficio";
						?>
						<div class="buscador small-12 medium-6 large-6" >
						    <div class="form-container" style="float: right; width: 100%; max-width: 450px;">
									<input type="submit" id="btn-send" class="rojo" value="" style="width: 76px; float: right; background: url(/wp-content/themes/flatsome-child/assets/images/btn-rojo.jpg) no-repeat; border: none;" />
									<input type="text" name="buscarbeneficio" id="buscarbeneficio" placeholder="<?php echo $placeholder ?>" style="max-width:350px;float: right;min-width: 250px;margin-right: 5px;height: 34px;" value="<?php if(isset($_GET["buscarbeneficio"])){ echo $placeholder; } ?>" />
							</div>
						</div>
					</div>
				</form>
			</div>
			
			<!-- VERSION MOBILE -->
			<div class="col small-12 large-12 medium-12 show-for-medium">
				<form action="" method="GET">
					<div class="row">
						<div class="col small-12 medium-12 large-12">
							<div class="select-container">

								<select name="filterCats" class="filterCats" id="filterCats">
									<option value="">Todos</option>
									<?php
									$taxonomy = 'categoria_beneficios';

									$taxonomy_terms = get_terms( $taxonomy, array(
										'hide_empty' => 0,
									) );
									foreach($taxonomy_terms as $term){
										if(isset($_GET["filterCats"]) && $_GET["filterCats"] == $term->term_id)
										{
											echo '<option value="'.$term->term_id.'" selected="selected">'.$term->name.'</option>';
										}
										else
										{
											echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';	
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col buscador small-12 medium-12 large-12" style="">
							<div class="col-inner">
								<div class="row">
									<div class="col small-9 medium-9">
										<div class="col-inner">
											<input type="text" name="buscarbeneficio" id="buscarbeneficio" placeholder="<?php echo $placeholder ?>" style="height: 34px;" value="<?php if(isset($_GET["buscarbeneficio"])){ echo $placeholder; } ?>" />
										</div>								
									</div>
									<div class="col small-3 medium-3">
										<div class="col-inner">
											<input type="submit" id="btn-send" class="rojo" value="" style="
												background: url(/wp-content/themes/flatsome-child/assets/images/btn-rojo.jpg) no-repeat;
												border: none; width:100% !important;" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<?php get_template_part( 'template-parts/beneficios/loop/beneficios' ); ?>
		

	</div>
	
<?php endwhile; ?>

</div>
<?php endif; ?>


<script type="text/javascript">
	jQuery(document).ready(function($) {

		$("#filterCats").on('change', function(event)
		{
/*			event.preventDefault();
			//$("a.filterCats").removeClass('active');
			//$(this).addClass('active');

			var $currentCat = $(this).children("option:selected").val(); //$(this).attr('rel');

			if( $currentCat.length > 0 ){
				$(".caja-beneficio").addClass('hidden');
				$(".caja-beneficio.categoria-" + $currentCat).removeClass('hidden');
			}else{
				$(".caja-beneficio").removeClass('hidden');
			}*/

		});


		/*$("#buscarbeneficio").on('keyup', function(event)
		{
			event.preventDefault();
			var searchValue = $(this).val();

			if( searchValue.length > 3 )
			{

				$(".caja-beneficio").addClass('hidden');

				$(".caja-beneficio").each(function()
				{

					if( $(this).text().search(searchValue) > -1)
					{
						$(this).removeClass('hidden');
					}

				});

			}else{

				$(".caja-beneficio").removeClass('hidden');

			}

		});*/
		
		$("#btn-send").on('click', function(event)
		{
/*			event.preventDefault();
			var searchValue = $("#buscarbeneficio").val();

			if( searchValue.length > 3 )
			{
                
                var txtBeneficio = searchValue.toLowerCase();
                
				$(".caja-beneficio").addClass('hidden');

				$(".caja-beneficio").each(function()
				{
                    var txtCurrentBox = $(this).text().toLowerCase();
                    console.log(txtCurrentBox);
                    console.log(txtBeneficio);
                    console.log("==============");
					if( txtCurrentBox.search(txtBeneficio) > -1)
					{
						$(this).removeClass('hidden');
					}

				});

			}else{

				$(".caja-beneficio").removeClass('hidden');

			}*/

		});

		/*$.ajax({
			type: 'GET',
			cache: true,
			url: '/wp-json/wp/v2/categoria_beneficios?per_page=100',
			dataType: 'json',
			success: function(cat_beneficios) {

				console.log(cat_beneficios);

                $.each(cat_beneficios, function(index, catBen) {

                    var str2div = '<li id="beneficio-'+catBen.id+'" >';
                    str2div += '<a class="filterCats" href="#'+catBen.slug+'">'+catBen.name+'</a>';
                    str2div += '</li>';

                    $(".categorias ul").append(str2div);

					//$(".categorias ul").append( $('li').addClass( 'beneficio-'+catBen.id ).add('a').addClass('filterCats').html(catBen.name).attr('href', catBen.slug) );

                });

			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});*/

	});
</script>


<?php get_footer(); ?>

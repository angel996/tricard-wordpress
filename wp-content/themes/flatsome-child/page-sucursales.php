<?php
/* Template Name: Página de Sucursales */
?>
<?php get_header(); ?>
<?php if ( have_posts() ): ?>
<div id="post-list">

	<?php echo do_shortcode('[block id="header-sucursales"]'); ?>

<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div class="section-content relative" style="margin-bottom: 150px;">
		<div class="row">
			<div class="col medium-12 small-12 large-12">
				<div class="col-inner">
					<div class="data-web-container hide-for-medium">
						
					</div>

					<div class="data-mobile-container show-for-small">
						
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>

</div>

<?php endif; ?>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		var slug				=	'region_comuna';
		var comunasPorRegion	=	new Array();

		var flagTest = false;
		var urlGoogleMaps = 'https://www.google.com/maps/search/';
		
		// Get regions
		$.ajax({
			type: 'GET',
			cache: true,
			url: '/wp-json/wp/v2/region_comuna?per_page=100',
			dataType: 'json',
			success: function(posts) {

				comunasPorRegion = posts;

                $.each(posts, function(index, post)
				{
					if( post.parent == 0){
						$('#region').append('<option value="' + post.id + '">' + post.name + '</option>');
					}
					else{
						//$('#comunas').append('<option value="' + post.id + '">' + post.name + '</option>');
					}

					// Preparacion de data para WEB responsive
					var str2table = '<div class="comunas hidden" id="comuna-'+post.id+'" data-slug="' + post.slug + '" data-name="' + post.name + '" data-parent="'+ post.parent +'" >';
					str2table += '<h2>'+post.name+'</h2>';

					str2table += '<table id="table-comuna-web-'+post.id+'"><thead>';
					//str2table += '	<tr><td>Nombre</td><td>Direccion</td><td>Estado</td><td>Horario</td></td></tr>';
					str2table += '	<tr class="sucursal-header"><td><p>Nombre</p></td><td><p>Direccion</p></td><td><p>Ver en el mapa</p></td><td><p>Estado</p></td><td><p>Horario</p></td></tr>';
					str2table += '</thead><tbody></tbody></table>';
					str2table += '</div>';

					$(".data-web-container").append( str2table );
					
					
					// Preparacion de data para MOBILE responsive
					str2table = '<div class="comunas hidden" id="comuna-'+post.id+'" data-slug="' + post.slug + '" data-name="' + post.name + '" data-parent="'+ post.parent +'" >';
					str2table += '<h2>'+post.name+'</h2>';

					str2table += '<table id="table-comuna-mobile-'+post.id+'"><thead>';
					str2table += '</thead><tbody></tbody></table>';
					str2table += '</div>';
					
					$(".data-mobile-container").append( str2table );
                });

                flagTest = true;

                for (var i = 1; i <= 2; i++)
				{
					$.ajax({
						type: 'GET',
						cache: true,
						url: '/wp-json/wp/v2/sucursales?order=asc&orderby=id&page='+i+'&per_page=100', //url: '/wp-json/wp/v2/sucursales?filter[region_comuna]='+post.slug,
						dataType: 'json',
						success: function(sucursales)
						{
							$.each(sucursales, function(index, sucursal)
							{
								var estado = "Abierto";
								var operational_time = sucursal.acf.horario.split("_");
								
								if(operational_time.length > 1)
								{
									var start_time = operational_time[1].split(" - ")[0];
									var end_time = operational_time[1].replace(" - ", " a ");
									end_time = end_time.split(" a ")[1];

									var start_time_aux = new Date();
									var end_time_aux = new Date();
									var current_time_aux = new Date();
									
									start_time_aux.setHours(start_time.replace(":00", ""),0,0);
									end_time_aux.setHours(end_time.replace(":00", ""),0,0);
//									current_time_aux.setHours("18",59,0); // Utilizado para pruebas

									if(start_time_aux <= current_time_aux && current_time_aux <= end_time_aux)
									{
										estado = "<p style='color:#078922'>Abierto</p>";
									}
									else
									{
										estado = "<p style='color:#F30000'>Cerrado";
									}
								}
								
								// WEB
								// Nombre, Direccion, Estado, Horario
								var elTr = '<tr class="sucursal-content">';
										elTr += '<td>'+sucursal.title.rendered+'</td>';
										elTr += '<td>'+sucursal.acf.direccion+'</td>';
										elTr += '<td><u><a target="_blank" href="'+urlGoogleMaps+encodeURIComponent(sucursal.acf.direccion)+'">'+sucursal.acf.direccion+'</a></u><span class="dashicons dashicons-location"></span></td>';
										elTr += '<td>'+estado+'</td>';
										elTr += '<td>'+sucursal.acf.horario.replace("_", "<br>")+' hrs</td>';
									elTr += '</tr>';

								$("#table-comuna-web-"+sucursal.region_comuna[0]+" tbody").append(elTr);
								

								// MOBILE
								// Nombre, Direccion, Estado, Horario
								var elTr = '<tr class="sucursal-content">';
										elTr += '<td>'+sucursal.title.rendered+'<br>'+sucursal.acf.direccion+'<br><u><a target="_blank" href="'+urlGoogleMaps+encodeURIComponent(sucursal.acf.direccion)+'">'+sucursal.acf.direccion+'</a></u><span class="dashicons dashicons-location"></span></td>';
										elTr += '<td>'+sucursal.acf.horario.replace("_", "<br>")+' hrs<br>'+estado+'</td>';
										elTr += '</tr>';

								$("#table-comuna-mobile-"+sucursal.region_comuna[0]+" tbody").append(elTr);
							});

						},
						error: function (request, status, error) {
							console.log(request.responseText);
						}
					});
				}

			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});


		$("#comunas").on('change', function(event) {
			event.preventDefault();

			if(this.value == "all")
			{
				var $idRegion = $("#region").val();

				$(".comunas").addClass('hidden')
				$(".comunas[data-parent="+ $idRegion +"]").removeClass('hidden');
			}
			else
			{
				var $idRegion = this.value;

				$(".comunas").addClass('hidden')
				//$(".comunas").find("[data-slug='" + $idRegion + "']").removeClass('hidden');
				$("#comuna-" + $idRegion).removeClass('hidden');
			}
		});

		$("#region").on('change', function(event) {
			event.preventDefault();
			var $idRegion = this.value;
			$("#comunas option").remove();
			$('#comunas').append('<option value="">-- Selecciona tu comuna --</option><option value="all">Todas las comunas</option>');

			$.each(comunasPorRegion, function(index, comuna)
			{

				 if( $idRegion == comuna.parent ){
				 	$('#comunas').append('<option value="' + comuna.id + '">' + comuna.name + '</option>');

				 } else {
				 }
			});
		});

		$("#sucursal").on('keyup', function(event)
		{
			event.preventDefault();
			var searchValue = $(this).val().toLowerCase();

			if( searchValue.length > 3 )
			{
				$(".comunas").addClass('hidden');
				$(".comunas table tr").addClass('hidden');

				$(".comunas").each(function()
				{	
					var $comunas = $(this);
					
					$comunas.find("table tr").each(function()
					{
						if( $(this).text().toLowerCase().search(searchValue) > -1)
						{
							$(this).removeClass('hidden');
							$(this).closest(".comunas").removeClass('hidden');
						}
					});
				});

			}else{

				$(".comunas").removeClass('hidden');

			}
		});
	});
</script>
<?php get_footer(); ?>

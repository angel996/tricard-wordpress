<?php
/* Template Name: Página de Mis beneficios */
?>
<?php get_header(); ?>

<?php if ( have_posts() ): ?>

<div id="post-list">
	
<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>


	<?php
	$args = array(
		'post_type'		=> 'page',
		'post_parent'	=> 47,
		'post_status'		=> 'publish',
		'posts_per_page'	=> 2,
		'paged'			=> get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'orderby'		=> 'title',
		'order'			=> 'ASC',
	);

	$loop = new WP_Query( $args );
	?>

	<section class="section section-page-seguros">
		<div class="row row-collapse align-center" id="row-1221666063">
			<div id="col-1318535001" class="col small-12 large-12">
				<div class="col-inner text-center">
					<div id="text-1221428842" class="text">
						<p>Te invitamos a conocer nuestros productos</p>

						<style>
							#text-1221428842 {
								font-size: 1.75rem;
								line-height: 1;
							}
							@media (min-width:550px) {
								#text-1221428842 {
									font-size: 2rem;
								}
							}
						</style>
					</div>
				</div>

				<style>
					#col-1318535001 > .col-inner {
						margin: 30px 0px 0px 0px;
					}
				</style>
			</div>

			<div id="col-1291405798" class="col small-12 large-12">
				<div class="col-inner">

					<div id="text-2444229601" class="text">
						<p>Asegura tu vida, nosotros nos encargamos del resto.</p>

						<style>
							#text-2444229601 {
								font-size: 1rem;
								line-height: 1.25;
								text-align: center;
							}
							@media (min-width:550px) {
								#text-2444229601 {
									font-size: 1.25rem;
								}
							}
						</style>
					</div>
				</div>

				<style>
					#col-1291405798 > .col-inner {
						margin: -20px 0px 0px 0px;
					}
					@media (min-width:550px) {
						#col-1291405798 > .col-inner {
							margin: -30px 0px 0px 0px;
						}
					}
				</style>
			</div>

		</div>
		<div class="row cajas-beneficios">
			<?php
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<div class="row large-columns-2 medium-columns- small-columns-1">
				<div class="page-col col">
					<div class="col-inner">
						<a class="plain" href="http://ec2-54-183-198-70.us-west-1.compute.amazonaws.com:8080/seguros/salud-total/" title="<?php print the_title(); ?>" target="">
							<div class="page-box box has-hover box-overlay dark box-text-middle">
								<div class="box-image">
									<div class="box-image image-cover" style="padding-top:160px;">
										<img width="300" height="69" src="http://ec2-54-183-198-70.us-west-1.compute.amazonaws.com:8080/wp-content/uploads/2021/01/seguros-salud-total-300x69.jpg" class="attachment-medium size-medium" alt="" loading="lazy" srcset="<?php the_post_thumbnail_url('square'); ?>" sizes="(max-width: 300px) 100vw, 300px">
									</div>
									<div class="overlay" style="background-color: rgba(0, 0, 0, 0.3)"></div>
								</div>
								<div class="box-text text-center is-xlarge">
									<div class="box-text-inner">
										<p><?php print the_title(); ?></p>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<?php endwhile; ?>
		</div><!-- .row.cajas-beneficios -->

		<div class="paginator-beneficios col col-cat medium-12 small-12 large-12">
			<div style="text-align:center">
				<?php
				// get the current page
				if ( !$current_page = get_query_var('paged') )
					$current_page = 1;

				if( get_option('permalink_structure') ) {
					$format = '?paged=%#%';
				}
				echo str_replace("<li><span", "<li class='current-page'><span", paginate_links(array(
					'base'     => get_pagenum_link(1) . '%_%',
					'format'   => $format,
					'current'  => $current_page,
					'total'    => $loop->max_num_pages,
					'mid_size' => 4,
					'prev_text' => '<img src="http://ec2-54-183-198-70.us-west-1.compute.amazonaws.com:8080/wp-content/uploads/2021/04/chevron-double-left-light.svg">',
					'next_text' => '<img src="http://ec2-54-183-198-70.us-west-1.compute.amazonaws.com:8080/wp-content/uploads/2021/04/chevron-double-right-light.svg">',
					'type'=>'list'
				)));
				?>
			</div>
		</div>

		<?php wp_reset_postdata(); ?>
	</section>
</div>
	
<?php endwhile; ?>

</div>
<?php endif; ?>


<?php get_footer(); ?>
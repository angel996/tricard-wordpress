
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_pmxi_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_pmxi_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `import_id` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci,
  `path` text COLLATE utf8mb4_unicode_520_ci,
  `registered_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_pmxi_files` WRITE;
/*!40000 ALTER TABLE `wp_pmxi_files` DISABLE KEYS */;
INSERT INTO `wp_pmxi_files` VALUES (2,1,'Regiones_y_comunas_Export_2021_January_04_1610.zip','/wpallimport/uploads/5a45941792d23ba3241bb25e3af46d2e/Regiones-y-comunas-Export-2021-January-04-1610.xml','2021-01-04 16:13:55'),(4,2,'Sucursales_Export_2021_January_04_1616.zip','/wpallimport/uploads/f7bacd60ef9342310c03d1b21accd098/Sucursales-Export-2021-January-04-1616.xml','2021-01-04 16:17:56'),(6,3,'Sucursales_Export_2021_January_04_1616.zip','/wpallimport/uploads/5ab4526f7c3dc749f6a25c87131dc198/Sucursales-Export-2021-January-04-1616.xml','2021-01-04 16:26:15'),(8,4,'Sucursales_Export_2021_January_04_1616.zip','/wpallimport/uploads/e3b9ed038b8aba485fec9682758cb2d6/Sucursales-Export-2021-January-04-1616.xml','2021-01-04 16:28:31');
/*!40000 ALTER TABLE `wp_pmxi_files` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


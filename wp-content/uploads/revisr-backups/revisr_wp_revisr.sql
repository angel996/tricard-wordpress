
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text COLLATE utf8_bin,
  `event` varchar(42) COLLATE utf8_bin NOT NULL,
  `user` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_revisr` WRITE;
/*!40000 ALTER TABLE `wp_revisr` DISABLE KEYS */;
INSERT INTO `wp_revisr` VALUES (1,'2021-09-23 18:01:17','Successfully created a new repository.','init','Angel Espinoza'),(2,'2021-09-23 18:02:04','Error pushing changes to the remote repository.','error','Angel Espinoza'),(3,'2021-09-23 18:15:04','Error backing up the database.','error','Angel Espinoza'),(4,'2021-09-23 18:15:04','Error staging files.','error','Angel Espinoza'),(5,'2021-09-23 18:15:04','There was an error committing the changes to the local repository.','error','Angel Espinoza'),(6,'2021-09-23 18:17:53','Error pushing changes to the remote repository.','error','Angel Espinoza'),(7,'2021-09-23 18:22:33','Error staging files.','error','Revisr Bot'),(8,'2021-09-23 18:22:47','Error backing up the database.','error','Revisr Bot'),(9,'2021-09-23 18:22:47','The daily backup was successful.','backup','Revisr Bot'),(10,'2021-09-23 19:51:23','Error pushing changes to the remote repository.','error','Angel Espinoza'),(11,'2021-09-23 19:53:22','Error pushing changes to the remote repository.','error','Angel Espinoza'),(12,'2021-09-24 11:54:46','Error backing up the database.','error','Angel Espinoza'),(13,'2021-09-24 11:59:48','Committed <a href=\"http://localhost:8888/tricot-2/wp-admin/admin.php?page=revisr_view_commit&commit=eab42e7&success=true\">#eab42e7</a> to the local repository.','commit','Angel Espinoza'),(14,'2021-09-24 12:31:28','Error pushing changes to the remote repository.','error','Angel Espinoza'),(15,'2021-09-24 12:32:59','Error pulling changes from the remote repository.','error','Angel Espinoza'),(16,'2021-09-24 12:35:20','Error pushing changes to the remote repository.','error','Angel Espinoza'),(17,'2021-09-24 12:36:54','Error pushing changes to the remote repository.','error','Angel Espinoza'),(18,'2021-09-24 12:37:44','Successfully pushed 1 commit to main/main.','push','Angel Espinoza'),(19,'2021-09-24 12:40:44','Error backing up the database.','error','Angel Espinoza'),(20,'2021-09-24 12:40:54','Error backing up the database.','error','Angel Espinoza'),(21,'2021-09-28 12:17:28','Error backing up the database.','error','Angel Espinoza'),(22,'2021-09-28 12:21:45','Successfully backed up the database.','backup','Angel Espinoza'),(23,'2021-09-28 12:23:12','Successfully pushed 1 commit to main/main.','push','Angel Espinoza'),(24,'2021-09-28 12:25:37','Successfully backed up the database.','backup','Angel Espinoza'),(25,'2021-09-28 12:25:38','Committed <a href=\"http://localhost:8888/tricot-2/wp-admin/admin.php?page=revisr_view_commit&commit=a6d09f1&success=true\">#a6d09f1</a> to the local repository.','commit','Angel Espinoza'),(26,'2021-09-28 14:55:44','Successfully backed up the database.','backup','Angel Espinoza'),(27,'2021-09-28 15:37:10','Successfully backed up the database.','backup','Angel Espinoza');
/*!40000 ALTER TABLE `wp_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


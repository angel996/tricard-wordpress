
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_aiowps_login_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_aiowps_login_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `logout_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_aiowps_login_activity` WRITE;
/*!40000 ALTER TABLE `wp_aiowps_login_activity` DISABLE KEYS */;
INSERT INTO `wp_aiowps_login_activity` VALUES (1,2,'Sebastian De La Fuente','2021-04-20 15:12:01','2021-04-21 14:36:14','200.111.224.205','',''),(2,2,'Sebastian De La Fuente','2021-04-21 15:26:15','1000-10-10 10:00:00','200.111.224.205','',''),(3,4,'barbara.urbina@ionix.cl','2021-04-21 17:01:42','1000-10-10 10:00:00','181.160.128.212','',''),(4,2,'Sebastian De La Fuente','2021-07-06 14:35:05','2021-07-14 11:57:09','172.20.12.6','',''),(5,2,'Sebastian De La Fuente','2021-07-12 13:43:16','2021-07-14 11:57:09','172.20.12.6','',''),(6,2,'Sebastian De La Fuente','2021-07-14 10:39:07','2021-07-14 11:57:09','172.20.12.6','',''),(7,2,'Sebastian De La Fuente','2021-07-14 11:58:33','2021-07-22 15:14:33','172.20.12.6','',''),(8,5,'Cristian Hernandez','2021-07-15 11:31:58','2021-08-02 13:45:30','172.20.12.6','',''),(9,2,'Sebastian De La Fuente','2021-07-19 13:42:50','2021-07-22 15:14:33','172.20.12.6','',''),(10,2,'Sebastian De La Fuente','2021-07-19 13:43:00','2021-07-22 15:14:33','172.20.12.6','',''),(11,2,'Sebastian De La Fuente','2021-07-22 08:55:00','2021-07-22 15:14:33','172.20.12.6','',''),(12,2,'Sebastian De La Fuente','2021-07-22 08:55:10','2021-07-22 15:14:33','172.20.12.6','',''),(13,2,'Sebastian De La Fuente','2021-07-22 15:06:36','2021-07-22 15:14:33','172.20.12.6','',''),(14,2,'Sebastian De La Fuente','2021-07-26 07:37:28','1000-10-10 10:00:00','172.20.12.6','',''),(15,2,'Sebastian De La Fuente','2021-07-28 08:34:37','1000-10-10 10:00:00','172.20.12.6','',''),(16,2,'Sebastian De La Fuente','2021-07-28 16:45:19','1000-10-10 10:00:00','172.20.12.6','',''),(17,2,'Sebastian De La Fuente','2021-07-31 23:36:25','1000-10-10 10:00:00','172.20.12.6','',''),(18,5,'Cristian Hernandez','2021-08-02 12:51:30','2021-08-02 13:45:30','172.20.12.6','',''),(19,2,'Sebastian De La Fuente','2021-08-03 06:14:20','1000-10-10 10:00:00','172.20.12.6','',''),(20,2,'Sebastian De La Fuente','2021-08-04 11:59:09','1000-10-10 10:00:00','172.20.12.6','',''),(21,2,'Sebastian De La Fuente','2021-08-08 12:28:07','1000-10-10 10:00:00','172.20.12.6','',''),(22,5,'Cristian Hernandez','2021-08-09 15:28:04','1000-10-10 10:00:00','172.20.12.6','',''),(23,2,'Sebastian De La Fuente','2021-08-10 13:25:37','1000-10-10 10:00:00','172.20.12.6','',''),(24,2,'Sebastian De La Fuente','2021-08-10 17:32:43','1000-10-10 10:00:00','172.20.12.6','',''),(25,2,'Sebastian De La Fuente','2021-08-11 12:01:53','2021-08-12 16:56:13','::1','',''),(26,2,'Sebastian De La Fuente','2021-08-13 09:50:13','2021-08-24 10:45:23','::1','',''),(27,2,'Sebastian De La Fuente','2021-08-15 17:05:15','2021-08-24 10:45:23','::1','',''),(28,2,'Sebastian De La Fuente','2021-08-18 08:24:30','2021-08-24 10:45:23','::1','',''),(29,2,'Sebastian De La Fuente','2021-08-20 12:39:07','2021-08-24 10:45:23','::1','',''),(30,2,'Sebastian De La Fuente','2021-08-22 12:45:58','2021-08-24 10:45:23','::1','',''),(31,2,'Sebastian De La Fuente','2021-08-24 10:09:54','2021-08-24 10:45:23','::1','',''),(32,6,'Angel Espinoza','2021-08-24 10:46:17','1000-10-10 10:00:00','::1','',''),(33,2,'Sebastian De La Fuente','2021-08-24 12:49:10','1000-10-10 10:00:00','::1','',''),(34,2,'Sebastian De La Fuente','2021-08-26 12:52:06','1000-10-10 10:00:00','::1','',''),(35,2,'Sebastian De La Fuente','2021-08-30 09:50:00','1000-10-10 10:00:00','::1','',''),(36,6,'Angel Espinoza','2021-09-01 10:38:22','1000-10-10 10:00:00','::1','',''),(37,6,'Angel Espinoza','2021-09-01 10:39:31','1000-10-10 10:00:00','::1','',''),(38,6,'Angel Espinoza','2021-09-03 10:40:42','1000-10-10 10:00:00','::1','',''),(39,6,'Angel Espinoza','2021-09-03 10:41:24','1000-10-10 10:00:00','::1','',''),(40,6,'Angel Espinoza','2021-09-06 08:35:34','1000-10-10 10:00:00','::1','',''),(41,2,'Sebastian De La Fuente','2021-09-08 09:12:10','1000-10-10 10:00:00','::1','',''),(42,2,'Sebastian De La Fuente','2021-09-10 14:43:21','1000-10-10 10:00:00','::1','',''),(43,2,'Sebastian De La Fuente','2021-09-12 22:26:37','1000-10-10 10:00:00','::1','',''),(44,2,'Sebastian De La Fuente','2021-09-15 08:23:50','1000-10-10 10:00:00','::1','',''),(45,6,'Angel Espinoza','2021-09-18 06:52:09','1000-10-10 10:00:00','::1','',''),(46,6,'Angel Espinoza','2021-09-20 17:25:31','1000-10-10 10:00:00','::1','',''),(47,6,'Angel Espinoza','2021-09-21 09:49:19','1000-10-10 10:00:00','::1','',''),(48,6,'Angel Espinoza','2021-09-22 14:52:12','1000-10-10 10:00:00','::1','',''),(49,6,'Angel Espinoza','2021-09-28 11:13:44','1000-10-10 10:00:00','::1','','');
/*!40000 ALTER TABLE `wp_aiowps_login_activity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


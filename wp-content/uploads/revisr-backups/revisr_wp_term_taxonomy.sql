
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,1),(2,2,'nav_menu','',0,8),(3,3,'region_comuna','Angol',86,1),(4,4,'region_comuna','Antofagasta',77,1),(5,5,'region_comuna','Arica',75,1),(6,6,'region_comuna','Buin',81,1),(7,7,'region_comuna','Calama',77,1),(8,8,'region_comuna','Ancud',88,1),(9,9,'region_comuna','Castro',88,1),(10,10,'region_comuna','Cauquenes',83,1),(11,11,'region_comuna','Cerrillos',81,1),(12,12,'region_comuna','Chillan',84,2),(13,13,'region_comuna','Colina',81,1),(14,14,'region_comuna','Concepcion',85,2),(15,15,'region_comuna','Constitucion',83,1),(16,16,'region_comuna','Copiapo',78,1),(17,17,'region_comuna','Coquimbo',79,2),(18,18,'region_comuna','Coronel',85,1),(19,19,'region_comuna','Coyhaique',89,1),(20,20,'region_comuna','Curico',83,1),(21,21,'region_comuna','El Bosque',81,1),(22,22,'region_comuna','Illapel',79,1),(23,23,'region_comuna','Independencia',81,1),(24,24,'region_comuna','Iquique',76,1),(25,25,'region_comuna','La Calera',80,2),(26,26,'region_comuna','La Florida',81,3),(27,27,'region_comuna','La Ligua',80,2),(28,28,'region_comuna','La Serena',79,4),(29,29,'region_comuna','La Union',87,1),(30,30,'region_comuna','Las Condes',81,3),(31,31,'region_comuna','Linares',83,2),(32,32,'region_comuna','Los Andes',80,2),(33,33,'region_comuna','Los Angeles',85,3),(34,34,'region_comuna','Macul',81,1),(35,35,'region_comuna','Maipu',81,2),(36,36,'region_comuna','Melipilla',81,1),(37,37,'region_comuna','Ñuñoa',81,1),(38,38,'region_comuna','Osorno',88,1),(39,39,'region_comuna','Ovalle',79,4),(40,40,'region_comuna','Parral',83,2),(41,41,'region_comuna','Peñaflor',81,1),(42,42,'region_comuna','Puente Alto',81,1),(43,43,'region_comuna','Puerto Montt',88,2),(44,44,'region_comuna','Puerto Varas',88,1),(45,45,'region_comuna','Punta Arenas',90,2),(46,46,'region_comuna','Quilicura',81,1),(47,47,'region_comuna','Quillota',80,3),(48,48,'region_comuna','Quilpue',80,2),(49,49,'region_comuna','Rancagua',82,3),(50,50,'region_comuna','Recoleta',81,1),(51,51,'region_comuna','Rengo',82,2),(52,52,'region_comuna','San Antonio',80,1),(53,53,'region_comuna','San Bernardo',81,1),(54,54,'region_comuna','San Carlos',84,2),(55,55,'region_comuna','San Felipe',80,2),(56,56,'region_comuna','San Fernando',82,2),(57,57,'region_comuna','San Javier',83,1),(58,58,'region_comuna','San Miguel',81,2),(59,59,'region_comuna','San Vicente',85,2),(60,60,'region_comuna','Santa Cruz',82,2),(61,61,'region_comuna','Santiago',81,6),(62,62,'region_comuna','Talagante',81,2),(63,63,'region_comuna','Talca',83,2),(64,64,'region_comuna','Talcahuano',85,1),(65,65,'region_comuna','Temuco',86,4),(66,66,'region_comuna','Tome',85,1),(67,67,'region_comuna','Valdivia',87,2),(68,68,'region_comuna','Vallenar',78,3),(69,69,'region_comuna','Valparaiso',80,2),(70,70,'region_comuna','Villa Alemana',80,2),(71,71,'region_comuna','Villarrica',86,1),(72,72,'region_comuna','Viña Del Mar',80,4),(73,73,'featured_item_category','',0,0),(74,74,'block_categories','',0,1),(75,75,'region_comuna','',0,0),(76,76,'region_comuna','',0,0),(77,77,'region_comuna','',0,0),(78,78,'region_comuna','',0,0),(79,79,'region_comuna','',0,0),(80,80,'region_comuna','',0,0),(81,81,'region_comuna','',0,0),(82,82,'region_comuna','',0,0),(83,83,'region_comuna','',0,0),(84,84,'region_comuna','',0,0),(85,85,'region_comuna','',0,0),(86,86,'region_comuna','',0,0),(87,87,'region_comuna','',0,0),(88,88,'region_comuna','',0,0),(89,89,'region_comuna','',0,0),(90,90,'region_comuna','',0,0),(91,91,'nav_menu','',0,7),(92,92,'nav_menu','',0,5),(93,93,'block_categories','',0,1),(94,94,'categoria_beneficios','',0,14),(95,95,'categoria_beneficios','',0,1),(96,96,'categoria_beneficios','',0,5),(97,97,'categoria_beneficios','',0,5),(98,98,'block_categories','',0,1),(103,103,'block_categories','UX BLOCKS para seguros',0,2);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


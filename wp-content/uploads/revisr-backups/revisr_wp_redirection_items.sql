
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_redirection_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_redirection_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `match_url` varchar(2000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `match_data` text COLLATE utf8mb4_unicode_520_ci,
  `regex` int(11) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `last_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_access` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'enabled',
  `action_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action_code` int(11) unsigned NOT NULL,
  `action_data` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `match_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `url` (`url`(191)),
  KEY `status` (`status`),
  KEY `regex` (`regex`),
  KEY `group_idpos` (`group_id`,`position`),
  KEY `group` (`group_id`),
  KEY `match_url` (`match_url`(191))
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_redirection_items` WRITE;
/*!40000 ALTER TABLE `wp_redirection_items` DISABLE KEYS */;
INSERT INTO `wp_redirection_items` VALUES (1,'/auth/home-page','/auth/home-page',NULL,0,0,5,'2021-07-26 10:35:04',1,'enabled','url',301,'/','url',NULL),(2,'/auth/requisito-page','/auth/requisito-page',NULL,0,1,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/tarjeta','url',NULL),(3,'/auth/tasa-comision-page','/auth/tasa-comision-page',NULL,0,2,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/tasas-y-comisiones','url',NULL),(4,'/auth/sucursal-page','/auth/sucursal-page',NULL,0,3,1,'2021-07-26 10:35:41',1,'enabled','url',301,'/buscador-de-sucursales','url',NULL),(5,'/auth/pago-cuenta-page','/auth/pago-cuenta-page',NULL,0,4,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/paga-tu-cuenta','url',NULL),(6,'/auth/contacto-page','/auth/contacto-page',NULL,0,5,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/contacto','url',NULL),(7,'/auth/bloqueo-tarjeta-page','/auth/bloqueo-tarjeta-page',NULL,0,6,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/bloquea-tu-tarjeta','url',NULL),(8,'/auth/corredora-page','/auth/corredora-page',NULL,0,7,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/seguros','url',NULL),(9,'/auth/app-page','/auth/app-page',NULL,0,8,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/descargar-app','url',NULL),(10,'/auth/seguridad-page','/auth/seguridad-page',NULL,0,9,1,'2021-07-26 10:34:54',1,'enabled','url',301,'/bloquea-tu-tarjeta','url',NULL),(11,'/auth/portabilidad-page','/auth/portabilidad-page',NULL,0,10,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/portabilidad-financiera','url',NULL),(12,'/auth/requisito-page','/auth/requisito-page',NULL,0,11,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/tarjeta','url',NULL),(13,'/auth/contrato-page','/auth/contrato-page',NULL,0,12,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/contratos','url',NULL),(14,'/auth/sucursal-page','/auth/sucursal-page',NULL,0,13,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/sucursales','url',NULL),(15,'/auth/pago-cuenta-page','/auth/pago-cuenta-page',NULL,0,14,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/paga-tu-cuenta','url',NULL),(16,'/auth/nosotros-page','/auth/nosotros-page',NULL,0,15,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/quienes-somos','url',NULL),(17,'/auth/preguntas-page','/auth/preguntas-page',NULL,0,16,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/preguntas-frecuentes','url',NULL),(18,'/auth/seguridad-page','/auth/seguridad-page',NULL,0,17,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/bloquea-tu-tarjeta','url',NULL),(19,'/auth/informacion-page','/auth/informacion-page',NULL,0,18,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/informacion-legal-y-financiera','url',NULL),(20,'/auth/estado-financiero-page','/auth/estado-financiero-page',NULL,0,19,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/estados-financieros','url',NULL),(21,'/tricot-2/error-2/','/tricot-2/error-2','{\"source\":{\"flag_regex\":false}}',0,20,0,'1970-01-01 00:00:00',1,'enabled','url',301,'/tricot-2/originacion/error-2/','url',NULL);
/*!40000 ALTER TABLE `wp_redirection_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


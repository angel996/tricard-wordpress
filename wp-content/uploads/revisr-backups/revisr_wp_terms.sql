
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0),(2,'Menu header','menu-header',0),(3,'Angol','angol',0),(4,'Antofagasta','antofagasta',0),(5,'Arica','arica',0),(6,'Buin','buin',0),(7,'Calama','calama',0),(8,'Ancud','ancud',0),(9,'Castro','castro',0),(10,'Cauquenes','cauquenes',0),(11,'Cerrillos','cerrillos',0),(12,'Chillán','chillan',0),(13,'Colina','colina',0),(14,'Concepción','concepcion',0),(15,'Constitucion','constitucion',0),(16,'Copiapó','copiapo',0),(17,'Coquimbo','coquimbo',0),(18,'Coronel','coronel',0),(19,'Coyhaique','coyhaique',0),(20,'Curicó','curico',0),(21,'El Bosque','el-bosque',0),(22,'Illapel','illapel',0),(23,'Independencia','independencia',0),(24,'Iquique','iquique',0),(25,'La Calera','la-calera',0),(26,'La Florida','la-florida',0),(27,'La Ligua','la-ligua',0),(28,'La Serena','la-serena',0),(29,'La Union','la-union',0),(30,'Las Condes','las-condes',0),(31,'Linares','linares',0),(32,'Los Andes','los-andes',0),(33,'Los Angeles','los-angeles',0),(34,'Macul','macul',0),(35,'Maipu','maipu',0),(36,'Melipilla','melipilla',0),(37,'Ñuñoa','nunoa',0),(38,'Osorno','osorno',0),(39,'Ovalle','ovalle',0),(40,'Parral','parral',0),(41,'Peñaflor','penaflor',0),(42,'Puente Alto','puente-alto',0),(43,'Puerto Montt','puerto-montt',0),(44,'Puerto Varas','puerto-varas',0),(45,'Punta Arenas','punta-arenas',0),(46,'Quilicura','quilicura',0),(47,'Quillota','quillota',0),(48,'Quilpue','quilpue',0),(49,'Rancagua','rancagua',0),(50,'Recoleta','recoleta',0),(51,'Rengo','rengo',0),(52,'San Antonio','san-antonio',0),(53,'San Bernardo','san-bernardo',0),(54,'San Carlos','san-carlos',0),(55,'San Felipe','san-felipe',0),(56,'San Fernando','san-fernando',0),(57,'San Javier','san-javier',0),(58,'San Miguel','san-miguel',0),(59,'San Vicente','san-vicente',0),(60,'Santa Cruz','santa-cruz',0),(61,'Santiago','santiago',0),(62,'Talagante','talagante',0),(63,'Talca','talca',0),(64,'Talcahuano','talcahuano',0),(65,'Temuco','temuco',0),(66,'Tomé','tome',0),(67,'Valdivia','valdivia',0),(68,'Vallenar','vallenar',0),(69,'Valparaiso','valparaiso',0),(70,'Villa Alemana','villa-alemana',0),(71,'Villarrica','villarrica',0),(72,'Viña Del Mar','vina-del-mar',0),(73,'Test I','test-i',0),(74,'Sucursales','sucursales',0),(75,'Región de Arica y Parinacota','region-de-arica-y-parinacota',0),(76,'Región de Tarapacá','region-de-tarapaca',0),(77,'Región de Antofagasta','region-de-antofagasta',0),(78,'Región de Atacama','region-de-atacama',0),(79,'Región de Coquimbo','region-de-coquimbo',0),(80,'Región de Valparaíso','region-de-valparaiso',0),(81,'Región Metropolitana de Santiago','region-metropolitana-de-santiago',0),(82,'Región del Libertador General Bernardo O’Higgins','region-del-libertador-general-bernardo-ohiggins',0),(83,'Región del Maule','region-del-maule',0),(84,'Región del Ñuble','region-del-nuble',0),(85,'Región del Biobío','region-del-biobio',0),(86,'Región de La Araucanía','region-de-la-araucania',0),(87,'Región de Los Ríos','region-de-los-rios',0),(88,'Región de Los Lagos','region-de-los-lagos',0),(89,'Región de Aysén del General Carlos Ibáñez del Campo','region-de-aysen-del-general-carlos-ibanez-del-campo',0),(90,'Región de Magallanes y la Antártica Chilena','region-de-magallanes-y-la-antartica-chilena',0),(91,'Footer 1','footer-1',0),(92,'Footer 2','footer-2',0),(93,'Formularios','formularios',0),(94,'Delivery','delivery',0),(95,'Salud','salud',0),(96,'Bienestar','bienestar',0),(97,'Belleza','belleza',0),(98,'Beneficios','beneficios',0),(103,'Seguros','ux_seguros',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


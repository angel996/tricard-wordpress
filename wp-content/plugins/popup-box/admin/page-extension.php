<?php
/**
 * Extansion version
 *
 * @package    Wow_Plugin
 * @subpackage
 * @copyright   Copyright (c) 2018, Dmytro Lobov
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$pro_url = 'https://wow-estore.com/item/popup-box-pro/';
$pro_button = '<a href="' . esc_url($pro_url) .'" target="_blank" class="button is-primary is-radiusless is-fullwidth is-normal">Get Pro Version</a>';

?>

<section class="section">

    <h1 class="title has-text-centered is-b-margin">GET MORE FEATURES WITH THE PLUGIN EXTENSION.</h1>

	<div class="theme-browser">
		<div class="themes container">

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Triggers
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/triggers.png">
                    <p>Add additional triggers to open a popup such as: <br/>
                        <strong>Exit, Hover, Right Click, Select Text.</strong> <br/>Make your popups more functional.</p>
                </div>
                <?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Forced Interaction
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/remove_close.png">
                    <p>Disabling the close button can be used in a variety of ways, from protecting content or areas of your site to forcing users to complete an action before continuing.</p>
                </div>
	            <?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Redirect visitor on close
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/redirect.png">
                    <p>Defines if the visitor gets redirected to another URL after popup gets closed.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Scheduling
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/schedule.png">
                    <p>Add scheduling options to your popups. With multiple schedule types, you can precisely schedule your popups in just a few minutes.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Animation
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/animation.png">
                    <p>33 different animations for show and hide popup and overlay. Make the appearance of your popup more attractive</p>
                </div>
				<?php echo $pro_button;?>
            </div>



            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Video Support
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/video.png">
                    <p>Create the ultimate WordPress Video Popup for your marketing videos in minutes - customize your player, control video autoplay and close.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Auto Close
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/autoclose.png">
                    <p>This will help you lock the screen for some time so that the user can read the necessary information.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Targeting for Users Role
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/users.png">
                    <p>Target specific sets of your users with new conditions such as User Roles & Login Checks.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                       Display on Taxonomy
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/display.png">
                    <p>The ability to display pop-ups only in taxonomies. Works with WooCommerce, EDD and other plugins with taxonomy.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

            <div class="theme">
                <div class="theme-id-container">
                    <h2 class="theme-name">
                        Multi language
                    </h2>
                </div>
                <div class="height_screen">
                    <img src="<?php echo plugin_dir_url(__FILE__);?>assets/img/language.png">
                    <p>The condition for display the popup depending on the language of the site. It is good to use if you have a website in several languages and you need to show different popups for a different language.</p>
                </div>
				<?php echo $pro_button;?>
            </div>

		</div>
	</div>

</section>
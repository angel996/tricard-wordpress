/**
 * Powered Modal Popups
 * ---------------------------------------------------
 * @file      Defines main jQuery plugin
 * @author    Dmytro Lobov
 * @copyright @author
 * @version   1.0
 */

(function($) {
  'use strict';

  // Error checking
  if (!$ || typeof $ === 'undefined') {
    return console.log(
        '[ModalPopup] No jQuery library detected. Load ModalPopup after jQuery has been loaded on the page.');
  }

  $.fn.ModalPopup = function(config) {

    let _default = {
      block_page: false,
      open_popup: 'click',
      open_delay: '0',
      open_distance: '0',
      open_popupTrigger: 'ds-open-popup-1',
      close_popupTrigger: 'ds-close-popup',
      auto_closePopup: false,
      auto_closeDelay: '0',
      redirect_onClose: false,
      redirect_URL: '',
      redirect_target: '_blank',
      cookie_enable: false,
      cookie_name: 'ds-popup-1',
      cookie_days: '30',

      // Overlay
      overlay_closesPopup: true,
      overlay_isVisible: true,
      overlay_animation: 'fadeIn',
      overlay_css: {
        'background': 'rgba(0, 0, 0, .75)',
      },

      // Popup
      popup_zindex: '999',
      popup_esc: true,
      popup_animation: 'fadeIn',
      popup_position: '-center',
      popup_css: {
        'width': '550px',
        'height': 'auto',
        'padding': '15px',
        'background': '#ffffff',
      },
      content_css: {
        'padding': '15px',
      },

      // Close Button
      close_isVisible: true,
      close_outer: false,
      close_position: '-topRight',
      close_type: '-text',
      close_content: 'Close',
      close_css: {
        'font-size': '12px',
        'color': '#000000',
      },

      // Mobile
      mobile_show: true,
      mobile_breakpoint: '480px',
      mobile_css: {
        'width': '100%',
      },

      // YooTube video
      video_enable: false,
      video_autoPlay: true,
      video_onClose: true,

    };

    return this.each(function() {
      let settings = $.extend(true, {}, _default, config);
      let prefix = 'ds-popup';
      let self = this;
      let popup_id = self.id;
      let popup = $(self).find('.' + prefix + '-wrapper');
      let content = $(self).find('.' + prefix + '-content');
      let overlay = createOverlay();
      let close = createCloseBtn();
      let video = videoHosting();

      function videoHosting() {
        let youtube = $(self).find('iframe[src*="youtube.com"]');
        let vimeo = $(self).find('iframe[src*="vimeo.com"]');
        if (youtube.length > 0) {
          return youtube;
        } else if (vimeo.length > 0) {
          return vimeo;
        } else {
          return false;
        }
      }

      styles();
      openPopupActions();
      closePopupActions();

      function openPopupActions() {
        let action = settings.open_popup;
        switch (action) {
          case 'click':
            clickOpenAction();
            break;
          case 'hover':
            hoverOpenAction();
            break;
          case 'auto':
            autoOpenAction();
            break;
          case 'scrolled':
            scrolledOpenAction();
            break;
          case 'rightclick':
            rightClickOpenAction();
            break;
          case 'selected':
            selectedOpenAction();
            break;
          case 'exit':
            exitOpenAction();
            break;
        }

      }

      function clickOpenAction() {
        let trigger = settings.open_popupTrigger;
        let triggers = '.' + trigger + ', a[href$="' + trigger + '"]';
        $(triggers).on('click', function(event) {
          event.preventDefault();
          openPopup();
        });
      }

      function hoverOpenAction() {
        let trigger = settings.open_popupTrigger;
        let triggers = '.' + trigger + ', a[href$="' + trigger + '"]';
        $(triggers).on('mouseover', function(event) {
          event.preventDefault();
          openPopup();
        });
      }

      function autoOpenAction() {
        setTimeout(function() {
          openPopup();
        }, settings.open_delay * 1000);
      }

      function scrolledOpenAction() {
        $(document).on('scroll', function() {
          let scrollTop = $(window).scrollTop();
          let docHeight = $(document).height();
          let winHeight = $(window).height();
          let scrollPercent = (scrollTop) / (docHeight - winHeight);
          let scrollPercentRounded = Math.round(scrollPercent * 100);
          if (scrollPercentRounded >= settings.open_distance) {
            openPopup();
            $(document).off('scroll');
          }
        });
      }

      function rightClickOpenAction() {
        $(document).on('contextmenu', function() {
          openPopup();
          return false;
        });
      }

      function selectedOpenAction() {
        $(document).on('mouseup', function(e) {
          let selected_text = ((window.getSelection && window.getSelection()) ||
              (document.getSelection && document.getSelection()) ||
              (document.selection && document.selection.createRange && document.selection.createRange().text));
          if (selected_text.toString().length > 2) {
            openPopup();
          }
        });
      }

      function exitOpenAction() {
        $(document).on('mouseleave', function(e) {
          if (e.clientY < 0) {
            openPopup();
            $(document).off('mouseleave');
          }
        });
      }

      function openPopup() {
        if (hideOnMobile() === false) {
          return;
        }
        $(self).addClass('ds-active');
        $(overlay).show();
        $(popup).show();
        if (settings.block_page) {
          $('html, body').addClass('no-scroll');
        }
        videoAutoPlay();
        autoClosePopup();
      }

      function hideOnMobile() {
        if (!settings.mobile_show) {
          let isMobile = $(window).width() <= parseInt(settings.mobile_breakpoint);
          if (isMobile) {
            return false;
          }
        }
      }

      function closePopup() {
        setCookie();
        $(self).removeClass('ds-active');
        videoStop();
        setTimeout(function() {
          $(overlay).hide();
          $(popup).hide();
        }, 600);
        $('html, body').removeClass('no-scroll');
        redirectOnClose();

      }

      function autoClosePopup() {
        if (settings.auto_closePopup) {
          setTimeout(function() {
            closePopup();
          }, settings.auto_closeDelay * 1000);
        }
      }

      function closePopupActions() {
        clickCloseAction(); // click custom button close
        closePopupESC();
        closePopupOverlay();
        // click Popup Close Button
        $(close).on('click', function() {
          closePopup();
        });

      }

      function clickCloseAction() {
        $('#' + popup_id + ' .' + settings.close_popupTrigger).on('click', function() {
          closePopup();
        });
      }

      function styles() {
        overlayStyle();
        contentStyle();
        popupStyle();
        closeStyle();

      }

      function overlayStyle() {
        if (overlay) {
          $(overlay).css(settings.overlay_css).attr('data-ds-effect', settings.overlay_animation);
          $(overlay).css('z-index', settings.popup_zindex - 3);
        }
      }

      function contentStyle() {
        if (content) {
          $(content).css(settings.content_css);
          $(content).css('z-index', settings.popup_zindex - 1);
        }
      }

      function popupStyle() {
        if (popup) {

          $(popup).
              css(settings.popup_css).
              addClass(settings.popup_position).
              attr('data-ds-effect', settings.popup_animation);
          $(popup).css('z-index', settings.popup_zindex - 2);
          let isMobile = $(window).width() <= parseInt(settings.mobile_breakpoint);
          if (isMobile) {
            $(popup).css(settings.mobile_css);
          }
          fixedPopupStyle();
        }
      }

      function fixedPopupStyle() {
        let height = $(popup)[0].style.height;
        let width = $(popup)[0].style.width;

        if (width === 'auto') {
          $(popup).css({
            'width': $(popup).outerWidth() + 'px',
          });
        }

        if (height === 'auto') {
          $(popup).css({
            'height': $(popup).outerHeight() + 'px',
          });
        }

        switch (settings.popup_position) {
          case '-center':
            $(popup).css({
              'bottom': '0',
              'right': '0',
            });
            break;
          case '-bottomCenter':
          case '-topCenter' :
            $(popup).css({
              'right': '0',
            });
            break;
        }

      }

      function closeStyle() {
        if (close) {
          $(close).
              addClass(settings.close_position + ' ' + settings.close_type).
              css(settings.close_css).
              attr('data-ds-close-text', settings.close_content);
          if (settings.close_outer) {
            $(close).addClass('-outer');
          }
          $(close).css('z-index', settings.popup_zindex);
          if (settings.close_type === '-tag' || settings.close_type === '-icon') {
            let fontSize = $(close)[0].style.fontSize;
            let box = parseInt(fontSize) + 5;
            $(close).css({
              'width': box + 'px',
              'height': box + 'px',
              'line-height': box + 'px',
            });
          }
        }
      }

      function createOverlay() {
        if (settings.overlay_isVisible) {
          $(self).prepend('<div class="' + prefix + '-overlay fadeIn"></div>');
          return $(self).find('.' + prefix + '-overlay');
        } else {
          return false;
        }
      }

      function createCloseBtn() {
        if (settings.close_isVisible) {
          $(popup).prepend('<div class="' + prefix + '-close"></div>');
          return $(popup).find('.' + prefix + '-close');
        } else {
          return false;
        }
      }

      function closePopupESC() {
        if (settings.popup_esc === true) {
          $(window).on('keydown', function(event) {
            if (event.key === 'Escape' || event.key === 'Esc') {
              closePopup();
            }
          });
        }
      }

      function closePopupOverlay() {
        if (settings.overlay_closesPopup === true) {
          $(overlay).on('click', function() {
            closePopup();
          });
        }
      }

      function redirectOnClose() {
        if (settings.redirect_onClose) {
          let redirectUrl = settings.redirect_URL;
          if (redirectUrl !== '' && redirectUrl.indexOf('http') > -1) {
            window.open(redirectUrl, settings.redirect_target);
          }
        }
      }

      // Youtube video auto play
      function videoAutoPlay() {
        if ((settings.video_enable === true) && (settings.video_autoPlay === true) && video) {
          let videoURL = $(video).attr('src');
          $(video).attr('src', videoURL + '?autoplay=1');
        }
      }

      // Youtube video stop
      function videoStop() {
        if ((settings.video_enable === true) && (settings.video_onClose === true) && video) {
          let videoURL = $(video).attr('src');
          videoURL = videoURL.split('?')[0];
          $(video).attr('src', videoURL + '?autoplay=0');
        }
      }

      function setCookie() {
        if (!settings.cookie_enable) {
          return;
        }
        let days = settings.cookie_days;
        let CookieDate = new Date();
        CookieDate.setTime(CookieDate.getTime() + (days * 24 * 60 * 60 * 1000));
        if (days > 0) {
          document.cookie = settings.cookie_name + '=yes; path=/; expires=' + CookieDate.toGMTString();
        } else {
          document.cookie = settings.cookie_name + '=yes; path=/;';
        }
      }

    });
  };

  for (let key in window) {
    if (key.indexOf('DS_Popup_') >= 0) {
      let val = window[key];
      $(val.selector).ModalPopup(val);
    }
  }

})(jQuery);
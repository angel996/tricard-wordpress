=== Popup Box - new WordPress popup plugin ===
Contributors: Wpcalc
Donate link: https://wow-estore.com/item/popup-box-pro/
Tags: popup, popups, pop-up, wordpress popup, popup builder
Requires at least: 3.2
Tested up to: 5.7
Requires PHP: 5.6
Stable tag: 1.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily create awesome WordPress popups with live preview and many options for styling and showing popups on the frontend.

== Description ==

= Awesome WordPress popups with live preview =

Popup Box - WordPress plugin for easily create popups with live preview. Create your own amazing pop-ups using many style options and display options.
The ability to insert shortcodes to the popup content allows you to use many plugins to create forms (for example Contact Form 7) and something else.

Options for location on the screen and functions for open  allow you to create popup for a different purposes.

= Popup Box features =
* Create unlimited popups;
* Live preview;
* Many custom style options;
* Triggers for opening: Auto open with delay, open via click, Open when page scrolls;
* Support inserting the shortcodes into content;
* Display a pop-up window only on special pages;
* Enable test mode;

= Use with other plugins to maximize your results =

* [Popup Box – new WordPress popup plugin](https://wordpress.org/plugins/popup-box/)
* [Counter Box – powerful creator of counters, timers and countdowns](https://wordpress.org/plugins/counter-box/)
* [Button Generator – easily Button Builder](https://wordpress.org/plugins/button-generation/)
* [Herd Effects – fake notifications and social proof plugin](https://wordpress.org/plugins/mwp-herd-effect/)
* [Floating Button](https://wordpress.org/plugins/floating-button/)
* [Side Menu Lite – add sticky fixed buttons](https://wordpress.org/plugins/side-menu-lite/)
* [Sticky Buttons – floating buttons builder](https://wordpress.org/plugins/sticky-buttons/)
* [Bubble Menu – circle floating menu](https://wordpress.org/plugins/bubble-menu/)
* [Float menu – awesome floating side menu](https://wordpress.org/plugins/float-menu/)
* [Modal Window – create modal window](https://wordpress.org/plugins/modal-window/)
* [Wow Skype Buttons](https://wordpress.org/plugins/mwp-skype/)
* [Border Menu](https://wordpress.org/plugins/border-menu/)
* [Slide Menu](https://wordpress.org/plugins/slide-menu/)

= Support =
Search for answers and ask your questions at [support center](https://wordpress.org/support/plugin/popup-box/)

== Installation ==
* Installation option 1: Find and install this plugin in the `Plugins` -> `Add new` section of your `wp-admin`
* Installation option 2: Download the zip file, then upload the plugin via the wp-admin in the `Plugins` -> `Add new` section. Or unzip the archive and upload the folder to the plugins directory `/wp-content/plugins/` via ftp
* Press `Activate` when you have installed the plugin via dashboard or press `Activate` in the in the `Plugins` list
* Go to `Popup Box` section that will appear in your main menu on the left
* Click `Add new` to create your first menu
* Setup your popup
* Click Save

== Screenshots ==
1. Frontend
2. Popup content
3. Popup Style
4. Settings
5. Display Rules

== Changelog ==
= 1.1.2 =
* Fixed: class for a close popup

= 1.1.1 =
* Fixed: Place close button outer

= 1.1 =
* Fixed: function for showing popup on specific pages

= 1.0.2 =
* Added: "document ready" to script

= 1.0.1 =
* Fixed: fix work with page builder plugins

= 1.0 =
* Initial release
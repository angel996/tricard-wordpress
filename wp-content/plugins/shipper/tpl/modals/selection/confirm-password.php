<?php
/**
 * Shipper modal templates: site selection, confirm password template
 *
 * @since 1.2
 * @package shipper
 */

$get_data     = wp_unslash( $_GET ); // phpcs:ignore WordPress.Security.NonceVerification.Recommended -- nonce is already verified.
$username     = shipper_get_dashboard_username();
$form_action  = shipper_get_dashboard_authentication_url();
$redirect_url = network_admin_url( 'admin.php?page=shipper-api&type=' . $type );
$auth_key     = ! empty( $get_data['set_apikey'] ) ? trim( $get_data['set_apikey'] ) : '';
$assets       = new Shipper_Helper_Assets();
?>

	<figure class="sui-box-logo" aria-hidden="true">
		<img
			src="<?php echo esc_url( $assets->get_asset( 'img/wpmudev.png' ) ); ?>"
			srcset="<?php echo esc_url( $assets->get_asset( 'img/wpmudev@2x.png' ) ); ?> 2x"
		>

	</figure>

	<button class="sui-button-icon sui-button-float--right shipper-cancel">
		<i class="sui-icon-close sui-md" aria-hidden="true"></i>
		<span class="sui-screen-reader-text">
			<?php esc_attr_e( 'Close the modal', 'shipper' ); ?>
		</span>
	</button>

	<h3 class="sui-box-title sui-lg">
		<?php esc_html_e( 'Confirm WPMU DEV Password', 'shipper' ); ?>
	</h3>

	<p class="shipper-description">
		<?php esc_html_e( 'As a security measure, you need to confirm your WPMU DEV password before continuing.' ); ?>
		<?php
		echo esc_html(
			sprintf(
				/* translators: %s: admin username. */
				__( 'Please enter the password for your connected account (%s) below.', 'shipper' ),
				$username
			)
		);
		?>
	</p>
</div>

<form
	action="<?php echo esc_url( $form_action ); ?>"
	method="POST"
	class="sui-box-body sui-box-body-slim sui-block-content-center">
	<div class="sui-form-field shipper-with-button-icon">
		<label for="shipper-wpmu-dev-password" id="shipper-wpmu-dev-password-label" class="sui-label">
			<?php esc_html_e( 'WPMU DEV Password', 'shipper' ); ?>
		</label>

		<input
			type="password"
			name="password"
			id="shipper-wpmu-dev-password"
			placeholder="<?php esc_attr_e( 'Enter your WPMU DEV password', 'shipper' ); ?>"
			class="sui-form-control"
			aria-labelledby="shipper-wpmu-dev-password-label"
		/>

		<button class="sui-button-icon shipper-password-eye" type="button" style="display: none">
			<i class="sui-icon-eye" aria-hidden="true"></i>
		</button>

		<input type="hidden" id="shipper-username" name="username" value="<?php echo esc_attr( $username ); ?>">
		<input type="hidden" id="shipper-redirect-url" name="redirect_url" value="<?php echo esc_url( $redirect_url ); ?>">

		<span id="shipper-wpmu-dev-password-error" class="sui-error-message" style="display: none;">
			<?php esc_html_e( 'The password you entered is incorrect. Please try again.', 'shipper' ); ?>
		</span>

		<span id="shipper-wpmu-dev-password-something-went-wrong" class="sui-error-message" style="display: none;">
			<?php esc_html_e( 'Something went wrong. Please try again.', 'shipper' ); ?>
		</span>
	</div>

	<div class="sui-box-footer sui-flatten sui-content-center">
		<button
			type="submit"
			class="sui-button shipper-confirm-password"
			data-auth-key="<?php echo esc_attr( $auth_key ); ?>"
			data-confirm-password="<?php echo esc_attr( wp_create_nonce( 'shipper_confirm_wpmudev_password' ) ); ?>"
		>
			<span class="sui-loading-text">
				<?php esc_html_e( 'Continue', 'shipper' ); ?>
				<i class="sui-icon-chevron-right" aria-hidden="true"></i>
			</span>
			<i class="sui-icon-loader sui-loading" aria-hidden="true"></i>
		</button>
	</div>
</form>
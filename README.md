# Tricard public site (tricard_spu)

Wordpress source code from Tricard
 

## First steps

### Create mysql docker image

Create database `tricot_spu` collation `utf8_unicode_ci` charset `utf8`
```
docker run -p 3306:3306 --name mysql -dit -e MYSQL_ROOT_PASSWORD=Tricot.123 -e MYSQL_DATABASE=tricot_spu mysql:5.6 --character-set-server=utf8 --collation-server=utf8_unicode_ci
```

Raise mysql server
```
docker run mysql
```



### Connect from wordpress composer

Configure network to external `mysql` image
```
docker network create dbnet
docker network connect dbnet mysql
```

Run docker compose
```
docker-compose up
```



## Create build for production

Get original wordpress files
```
sudo docker run --rm --volumes-from tricot -v $(pwd):/backup wordpress tar zcvf /backup/wordpress-files-backup.tar.gz /var/www/html
```

Run dockerFile from current working directory
```
sudo docker build -t tricard_spu .
```



## Admin credentials
```
user: Sebastian De La Fuente
pass: ionix2021
```# tricard-wordpress
